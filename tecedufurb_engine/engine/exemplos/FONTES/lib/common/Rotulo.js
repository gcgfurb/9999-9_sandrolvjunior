/*
	Classe que representa a demarcacao dos 'Eixo' na cena
	
	Descricao: Eixos paralelos para demarcacao de valores
*/
var RotulosNumericos = function(t){
	var geometria,
		rotulos,
        r;
	var geo = [];

    //Criar um Layer para adicionar cada rotulo separadamente.
    var layer = new Layer().initialize();

	geometria = new THREE.Geometry();

		for(var i = -15; i<16; i++){
			if(i != 0){
                geo[0] = new THREE.Vector3(i, 0, 0.1);
                geo[1] = new THREE.Vector3( i, 0, -0.1 );
                r = ThreeJSBuilder.createLinesObject(0, 0, 0, geo, 0xFF0000);
                layer.addGameObject(r);
                geo[0] = new THREE.Vector3(-0.1, i, 0);
                geo[1] = new THREE.Vector3( 0.1, i, 0 );
                r = ThreeJSBuilder.createLinesObject(0, 0, 0, geo, 0x00cc00);
                layer.addGameObject(r);
                geo[0] = new THREE.Vector3(-0.1, 0, i);
                geo[1] = new THREE.Vector3( 0.1, 0, i );
                r = ThreeJSBuilder.createLinesObject(0, 0, 0, geo, 0x0000FF);
                layer.addGameObject(r);
            }
		}
	

		/*for(var i = -15; i<16; i++){
			if(i != 0){
				geometria.colors.push(new THREE.Color( 0xFF0000 ), new THREE.Color( 0xFF0000 ));
				geometria.colors.push(new THREE.Color( 0x00cc00 ), new THREE.Color( 0x00cc00 ));
				geometria.colors.push(new THREE.Color( 0x0000FF ), new THREE.Color( 0x0000FF ));
			}
		}*/

	layer.threeObject.name = "rotulos";
	layer.threeObject.type = THREE.Line;
		
	this.getRotulos = function(){
		return layer;
	};
};