/**
 * Created by gabriel on 02/03/2016.
 */

function Query3DComponent(){}

Query3DComponent.prototype = new Component();

Query3DComponent.prototype.Projector3D = new THREE.Projector();

Query3DComponent.prototype.Raycaster = new THREE.Raycaster();

Query3DComponent.prototype.mouse3D = {x: 0, y: 0};

Query3DComponent.prototype.query = function(x,y){
    //Captura a posicao X do mouse3D, descontando a altura do menu superior
    this.mouse3D.x = ((x - controles3D.screen.left) / Game.apiHandler.renderer.width) * 2 - 1;
    //Captura a posicao Y do mouse3D, descontando a largura da lista de equacoes
    this.mouse3D.y = -((y - controles3D.screen.top) / Game.apiHandler.renderer.height) * 2 + 1;

    //Cria um raio com origen na posicao do mouse3D em direcao a camera3D (dentro da cena3D)
    var vetor = new THREE.Vector3( this.mouse3D.x, this.mouse3D.y, 1 );
    this.Projector3D.unprojectVector( vetor, camera3D.getCamera3D() );
    this.Raycaster.set( camera3D.getPosicao(), vetor.sub( camera3D.getPosicao() ).normalize() );

    // cria um array com todos os objetos da cena3D que sao interseccionados pelo raio.
    var intersects = this.Raycaster.intersectObjects( this.owner.threeObject.children );
    return intersects[0];

    /*if ( intersects.length > 0 ){
        atualizaBBoxSelecionada();
        OBJETO_SELECIONADO = scene.threeObject.getObjectByName(intersects[0].object.name);
        //Objeto precisa estar visivel
        if(OBJETO_SELECIONADO.getMalhaGraficaFuncaoThreeJS().material.visible == true ){
            //Desenha a BoundingBox
            OBJETO_SELECIONADO.getBoundingBox();
            //Seleciona a equacao na lista de equacoes
            OBJETO_SELECIONADO.selectEquacaoNaLista();
            atualizaPainelObjSelecionado();
        }
    }else{
        atualizaBBoxSelecionada();
    }*/
};

Query3DComponent.prototype.getSystems = function(){
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, LogicSystem);
    return systems;
};

Query3DComponent.prototype.getTag = function(){
    return "QUERY_3D";
};