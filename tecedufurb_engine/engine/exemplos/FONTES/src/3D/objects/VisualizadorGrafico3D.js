//Executa a funcao 'main' assim que a pagina e carregada
window.onload = main;

var camera3D, 
	cena3D,
	rotulos3D,
	renderizador3D, 
	controles3D,	
	projetor3D,
	raio3D,
	container,
	mouse3D = {x: 0, y: 0},
	fps,
	armazenadorLocal;
var scene;
var layer;
	
//Indice para controle da lista de equacoes e serve tambem como nome de cada objetoGrafico	
var indice3D = 0;

//Lista de equacoes existentes na cena3D
var funcaoList3D = new Array();	
	
//variavel que tem o objeto selecionado a partir do duplo clique do mouse3D	
var OBJETO_SELECIONADO;	

function main(){
	inicia();
	//anima();
};

function inicia(){
	//Detecta qual e o browser usado
	BrowserDetect.init();

    //Deccide qual api vou usar na engine
    Game.loadAPI(new ThreeJSHandler());
    //Escolhe a div onde sera carragado o canvas
    var world = $('.drawAreaStyle');
    console.log("Usando Engine");
    //Cria a cena3D na engine
    scene = new Scene().initialize(-1000, -1000, 1000, 1000);

    //!@@ Ainda nao sei para que server essa parte.
    layer = new Layer().initialize();
    scene.addLayer(layer);
    ComponentUtils.addComponent(layer, new Query3DComponent().initialize());

    Game.init(world, scene);

    var aspect = world.width() / (world.height());

    Game.apiHandler.renderer.setClearColor(0xF6F6F6);
    Game.apiHandler.renderer.setSize(900, 700);
    Game.apiHandler.renderer.domElement.ondblclick = selecionaObjeto;

	/*//Cria a cena3D
	cena3D = new THREE.Scene();*/

	//Adiciona a camera3D peagando a que já é criada na engine.
	camera3D = new Camera3D(aspect);
	//cena3D.add(camera3D.getCamera3D());

    var grid = ThreeJSBuilder.createGridObject(0,-0.001,0, 15, 1);
    Game.apiHandler.addGameObject(grid, scene);
    grid.threeObject.name = "grade";

    var axis = ThreeJSBuilder.createAxisObject(0,0,0,17);
    Game.apiHandler.addGameObject(axis, scene);
    axis.threeObject.name = "eixo";

    var eixos_n = new Eixos3D();
    Game.apiHandler.addGameObject(eixos_n.getEixos(), scene);

    var rotulos3D = new RotulosNumericos("3D");
    Game.apiHandler.addGameObject(rotulos3D.getRotulos(), scene);

    var luzes = new Luzes3D();
    scene.addLight(luzes.getLuz("x"));
    scene.addLight(luzes.getLuz("y"));
    scene.addLight(luzes.getLuz("z"));

	controles3D = new THREE.TrackballControls(camera3D.getCamera3D(), Game.apiHandler.renderer.domElement);
	controles3D.damping = 0.2;

    Game.apiHandler.beforeRender = function(){
		controles3D.update();
    };
	
	/*
	//Cria o projetor3D da cena3D
	projetor3D = new THREE.Projector(); 
	
	//Cria o raio intersectador
	raio3D = new THREE.Raycaster();

	
	//Inicia o armazenador local e carrega a lista de equacoes
	armazenadorLocal = new FuncaoStorer();
	armazenadorLocal.carregaListaFuncao("3D");
	*/
};

function selecionaObjeto( event ){
    var busca = ComponentUtils.getComponent(layer, "QUERY_3D");
    var objeto = busca.query(event.clientX, event.clientY);
    if (objeto){
        atualizaBBoxSelecionada();
        OBJETO_SELECIONADO = funcaoList3D[objeto.object.name];
        //Objeto precisa estar visivel
        if(OBJETO_SELECIONADO.getMalhaGraficaFuncaoThreeJS().material.visible == true ){
            //Desenha a BoundingBox
            OBJETO_SELECIONADO.getBoundingBox();
            //Seleciona a equacao na lista de equacoes
            OBJETO_SELECIONADO.selectEquacaoNaLista();
            atualizaPainelObjSelecionado();
        }
    }else{
        atualizaBBoxSelecionada();
    }

};

//Se clicou fora e o objeto tiver a boundinBox, ele remove e atualiza o painel de selecao de objetos
function atualizaBBoxSelecionada(){
	if(OBJETO_SELECIONADO != null){
		var obj = OBJETO_SELECIONADO.getMalhaGraficaFuncaoThreeJS().getObjectByName("boundingbox");
		if(obj != undefined){
			OBJETO_SELECIONADO.removeBoundingBox();
			OBJETO_SELECIONADO.unSelectEquacaoNaLista();
			OBJETO_SELECIONADO = null;
			atualizaPainelObjSelecionado();
		}
	}
};

function atualizaPainelObjSelecionado(){
	var div, combo, chkbox;
	
	if(OBJETO_SELECIONADO != null){
		div = document.getElementById("div_objetoSelecionado");
		div.innerHTML = "&nbsp;Objeto: "+ OBJETO_SELECIONADO.getMalhaGraficaFuncaoThreeJS().name;
		combo = document.getElementById("comboBox_cores");
		combo.disabled = false;
		combo = document.getElementById("comboBox_material");
		combo.disabled = false;
		chkbox = document.getElementById("checkbox_wireframe");
		chkbox.disabled = false;
		$( "#slider-range_2" ).slider('enable');
		$( "#slider-range_1" ).slider('enable');
		if(OBJETO_SELECIONADO.getDominioDaFuncao() == 'y'){
			document.getElementById('range_1').innerHTML = "&nbsp;X: ";
			document.getElementById('range_2').innerHTML = "&nbsp;Z: ";	
		}else if(OBJETO_SELECIONADO.getDominioDaFuncao() == 'x'){	
			document.getElementById('range_1').innerHTML = "&nbsp;Z: ";	
			document.getElementById('range_2').innerHTML = "&nbsp;Y: ";	
		} else {
			document.getElementById('range_1').innerHTML = "&nbsp;X: ";	
			document.getElementById('range_2').innerHTML = "&nbsp;Y: ";	
		}
	}else{		
		div = document.getElementById("div_objetoSelecionado");
		div.innerHTML = "&nbspObjeto: ";
		combo = document.getElementById("comboBox_cores");
		combo.disabled = true;
		combo = document.getElementById("comboBox_material");
		combo.disabled = true;
		chkbox = document.getElementById("checkbox_wireframe");
		chkbox.disabled = true;
		$( "#slider-range_2" ).slider('disable');
		$( "#slider-range_1" ).slider('disable');
	}
	atualizaCorObjSelecionado();
	atualizaMaterialObjSelecionado();
	atualizaRangeValoresXY();
};


function atualizaCorObjSelecionado(){
	var combo = document.getElementById("comboBox_cores");
	
	if(OBJETO_SELECIONADO != null){
		if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "ff0000"){
			combo.selectedIndex = 1;
		}else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "00ff00"){
			combo.selectedIndex = 2;
		}else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "0000ff"){
			combo.selectedIndex = 3;
		}else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "ffff00"){
			combo.selectedIndex = 4;
		}else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "ff6600"){
			combo.selectedIndex = 5;
		} else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "9900ff"){
			combo.selectedIndex = 6;
		} else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().color.getHexString() == "666666"){
			combo.selectedIndex = 7;
		}
	}else{
		combo.selectedIndex = 0;
	}
};

//Ao selecionar um objeto grafico, ele atualiza o comboBox de material de acordo com o material do mesmo
function atualizaMaterialObjSelecionado(){
	var combo = document.getElementById("comboBox_material");
	var chkbox = document.getElementById("checkbox_wireframe");
	
	if(OBJETO_SELECIONADO != null){
		if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica() instanceof THREE.MeshLambertMaterial){
			combo.selectedIndex = 1;
		} else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica() instanceof THREE.MeshBasicMaterial){
			combo.selectedIndex = 2;
		} else if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica() instanceof THREE.MeshPhongMaterial){
			combo.selectedIndex = 3;
		} 
		
		//Se estiver exibindo em wireframe
		if(OBJETO_SELECIONADO.getMaterialEquacaoGrafica().wireframe == false){
			chkbox.checked = false;
		}else{
			chkbox.checked = true;
		}
	}else{
		combo.selectedIndex = 0;
	}
};

//Ao selecionar um objeto grafico, ele atualiza o slider X e Y de acordo com os valores do mesmo
function atualizaRangeValoresXY(){
	if(OBJETO_SELECIONADO != null){
		$( "#slider-range_1" ).slider("values", 0, OBJETO_SELECIONADO.getRange1min());
		$( "#slider-range_1" ).slider("values", 1, OBJETO_SELECIONADO.getRange1max());
		$( "#in_range_1" ).val( $( "#slider-range_1" ).slider( "values", 0 ) + "   |   " + $( "#slider-range_1" ).slider( "values", 1 ) );
		$( "#slider-range_2" ).slider("values", 0, OBJETO_SELECIONADO.getRange2min());
		$( "#slider-range_2" ).slider("values", 1, OBJETO_SELECIONADO.getRange2max());
		$( "#in_range_2" ).val( $( "#slider-range_2" ).slider( "values", 0 ) + "   |   " + $( "#slider-range_2" ).slider( "values", 1 ) );
	}else{ 
		$( "#slider-range_1" ).slider("values", 0, -2);
		$( "#slider-range_1" ).slider("values", 1, 2);
		$( "#in_range_1" ).val( $( "#slider-range_1" ).slider( "values", 0 ) + "   |   " + $( "#slider-range_1" ).slider( "values", 1 ) );		
		$( "#slider-range_2" ).slider("values", 0, -2);
		$( "#slider-range_2" ).slider("values", 1, 2);
		$( "#in_range_2" ).val( $( "#slider-range_2" ).slider( "values", 0 ) + "   |   " + $( "#slider-range_1" ).slider( "values", 1 ) );
	}   
};

//Redefine a posicao da camera3D para inicial
function redefineCamera(){
	controles3D.reset();
};
