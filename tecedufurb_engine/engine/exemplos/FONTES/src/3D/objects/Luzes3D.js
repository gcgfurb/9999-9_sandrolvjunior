/*

	Classe que representa o 'Controle' na cena (Zoom, Rotacao, Tralacao etc).
	
*/
var Luzes3D = function(){
	var lightX, lightY, lightZ;

    //Adiciona luz vertical na cena3D (X)
	lightX = ThreeJSBuilder.createDirectionalLightObject(32, 0, 0, 0, 0, 0, 0xffffff, 1.0);
    lightX.threeObject.name = "luzX";

    //Adiciona luz horizontal na cena3D (Y)
    lightY = ThreeJSBuilder.createDirectionalLightObject(0, 32, 0, 0, 0, 0, 0xffffff, 1.0);
	lightY.threeObject.name = "luzY";

    //Adiciona luz frontal na cena3D (Z)
    lightZ = ThreeJSBuilder.createDirectionalLightObject(0, 0, 32, 0, 0, 0, 0xffffff, 1.0);
    lightZ.threeObject.name = "luzZ";

	this.getLuz = function(direcao){
		if(direcao == "x"){
			return lightX;
		}else if(direcao == "y"){
			return lightY;
		}else if(direcao == "z"){
			return lightZ;
		}
	};	
	
};