/**
 * Created by gabri on 21/02/2016.
 */

function ParametricGeometryComponent(){}

ParametricGeometryComponent.prototype = new RenderableComponent();

JSUtils.addMethod(ParametricGeometryComponent.prototype, "initialize",
    function(funcaoDeMalha, nivelDetalhe, cor){
        this.initialize();
        this.funcaoDeMalha = funcaoDeMalha;
        this.nivelDetalhe = nivelDetalhe;
        this.cor = cor;
        return this;
    }
);


ParametricGeometryComponent.prototype.getSystems = function(){
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, RenderSystem);
    return systems;
};


ParametricGeometryComponent.prototype.getTag = function(){
    return "PARAMETRICGEOMETRY_RENDER_COMPONENT";
};

ParametricGeometryComponent.prototype.genThreeObject = function(){
    var geometria = new THREE.ParametricGeometry(this.funcaoDeMalha, this.nivelDetalhe, this.nivelDetalhe);
    var material = new THREE.MeshLambertMaterial( { color: this.cor, side: THREE.DoubleSide } );
    var mesh = new THREE.Mesh(geometria, material);
    return mesh;
};