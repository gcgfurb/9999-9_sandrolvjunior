function MovePlayerComponent()
{
}
MovePlayerComponent.prototype = new Component();
MovePlayerComponent.prototype.onKeyDown = function(keyCode)
{
	if (keyCode == 37)
	{
		this.owner.setLinearVelocityX(-150);
		var anim = ComponentUtils.getComponent(this.owner, "ANIMATION_RENDER_COMPONENT");
		if (anim.animation != 9)
		{
			anim.play("HORIZONTAL", 9, 0, 8, -1, 50);
		}
	}
	else if (keyCode == 39)
	{
		this.owner.setLinearVelocityX(150);
		var anim = ComponentUtils.getComponent(this.owner, "ANIMATION_RENDER_COMPONENT");
		if (anim.animation != 11)
		{
			anim.play("HORIZONTAL", 11, 0, 8, -1, 50);
		}
	}
	else if (keyCode == 38)
	{
		this.owner.setLinearVelocityY(-500);
	}
}
MovePlayerComponent.prototype.onKeyUp = function(keyCode)
{
	if (keyCode == 37)
	{
		this.owner.setLinearVelocityX(0);
		ComponentUtils.getComponent(this.owner, "ANIMATION_RENDER_COMPONENT").play("HORIZONTAL", 2, 6, 6, -1, 150);
	}
	else if (keyCode == 39)
	{
		this.owner.setLinearVelocityX(0);
		ComponentUtils.getComponent(this.owner, "ANIMATION_RENDER_COMPONENT").play("HORIZONTAL", 2, 6, 6, -1, 150);
	}
	else if (keyCode == 38)
	{
		this.owner.setLinearVelocityY(0);
	}
}

MovePlayerComponent.prototype.getSystems = function()
{
	var systems = new Array();
	systems = ArrayUtils.addElement(systems, KeySystem);
	return systems;
}
MovePlayerComponent.prototype.getTag = function()
{
	return "MOVE_PLAYER_COMPONENT";
}

/*
******************************************************************************************
*/

var MovePlayerComponentParser = function() {
	this.parse = function(object) {
		var data = {
			className : "MovePlayerComponent",
		}

		return data;
	};

	this.unparse = function(data) {

		return new MovePlayerComponent();
	};

};

MovePlayerComponent.prototype._className = "MovePlayerComponent";
MetadataManager.addParser("MovePlayerComponent", MovePlayerComponentParser);
