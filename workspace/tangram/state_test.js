var stateString = "";

function saveState()
{
	
	stateString = JSON.stringify(MetadataManager.parse(Game));
	console.log(stateString);
}

function loadState()
{
	MetadataManager.parse(JSON.parse(stateString));
}

function buildGame()
{
	Game.loadAPI();
	AssetStore.addAsset(new Asset().initialize("SPRITE_PLAYER", "./sprite.png", "IMAGE"));
	AssetStore.addAsset(new Asset().initialize("GROUND_IMAGE", "./block.png", "IMAGE"));
	AssetStore.addAsset(new Asset().initialize("BACKGROUND_IMAGE", "./background.jpg", "IMAGE"));
	AssetStore.addAsset(new Asset().initialize("BEEP_AUDIO", "./beep-02.wav", "AUDIO"));

	var p1 = new PolygonObject().initialize(430, 200, 0,
		[
				new Point2D().initialize(-50, 100), new Point2D().initialize(-50, -100), new Point2D().initialize(50, 0)
		], null, "purple", "black");

	var p2 = new PolygonObject().initialize(500, 200, 0,
		[
				new Point2D().initialize(-100, -50), new Point2D().initialize(100, -50), new Point2D().initialize(0, 50)
		], null, "red", "black");

	var p3 = new PolygonObject().initialize(600, 200, 0,
		[
				new Point2D().initialize(-25, 0), new Point2D().initialize(25, -50), new Point2D().initialize(25, 50)
		], null, "pink", "black");

	var p4 = new PolygonObject().initialize(700, 300, 0,
		[
				new Point2D().initialize(25, 25), new Point2D().initialize(-25, 75), new Point2D().initialize(-25, -25), new Point2D().initialize(25, -75)
		], null, "yellow", "black");

	var p5 = new PolygonObject().initialize(550, 300, 0,
		[
				new Point2D().initialize(-50, 25), new Point2D().initialize(0, -25), new Point2D().initialize(50, 25)
		], null, "orange", "black");

	var p6 = new PolygonObject().initialize(500, 300, 0,
		[
				new Point2D().initialize(-50, 0), new Point2D().initialize(0, -50), new Point2D().initialize(50, 0), new Point2D().initialize(0, 50)
		], null, "green", "black");

	var p7 = new PolygonObject().initialize(550, 300, 0,
		[
				new Point2D().initialize(50, 25), new Point2D().initialize(-50, 25), new Point2D().initialize(50, -75)
		], null, "blue", "black");

	var ground = new BoxObject().initialize(500, 550, 0, 1888, 235, "red", "black");
	var background = new GameObject().initialize(500, 300, 0, 1920, 1200, 0);
	var player = new BoxObject().initialize(200, 300, 0, 25, 64, "red", "red");

	ComponentUtils.addComponent(ground, new RigidBodyComponent().initialize(0, 0, false, true, 0.2));
	ComponentUtils.removeComponent(ground, ComponentUtils.getComponent(ground, "BOX_RENDER_COMPONENT"));
	ComponentUtils.addComponent(player, new RigidBodyComponent().initialize(0, 1, true, false, 0.2));
	ComponentUtils.addComponent(player, new MovePlayerComponent().initialize());
	ComponentUtils.addComponent(player, new AnimationRenderComponent().initialize(AssetStore.getAsset("SPRITE_PLAYER").getAssetInstance(), 13, 21));
	ComponentUtils.removeComponent(player, ComponentUtils.getComponent(player, "BOX_RENDER_COMPONENT"));
	ComponentUtils.getComponent(player, "ANIMATION_RENDER_COMPONENT").play("HORIZONTAL", 2, 6, 6, -1, 150);
	ComponentUtils.addComponent(p1, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p2, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p3, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p4, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p5, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p6, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(p7, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
	ComponentUtils.addComponent(Game, new MoveCameraComponent().initialize(65, 68, 87, 83, 33, 34, 72, 75, 20, 0.1, 5));
	ComponentUtils.addComponent(Game, new FpsMeterComponent().initialize());
	ComponentUtils.addComponent(background, new ImageRenderComponent().initialize(AssetStore.getAsset("BACKGROUND_IMAGE").getAssetInstance(), false, "HORIZONTAL"));
	ComponentUtils.addComponent(ground, new ImageRenderComponent().initialize(AssetStore.getAsset("GROUND_IMAGE").getAssetInstance(), true, "HORIZONTAL"));

	//collabComp = new CollaborationComponent().initialize(new TogetherJSCollaborationStrategy(), document.getElementById("box"));
	//ComponentUtils.addComponent(Game, collabComp);

	var layer = new Layer().initialize();
	layer.setGravity(0);
	var layer2 = new Layer().initialize();
	var scene = new Scene().initialize(-4000, -4000, 4000, 4000);

	layer.addGameObject(ground);
	layer.addGameObject(player);
	layer.addGameObject(p1);
	layer.addGameObject(p2);
	layer.addGameObject(p3);
	layer.addGameObject(p4);
	layer.addGameObject(p5);
	layer.addGameObject(p6);
	layer.addGameObject(p7);

	layer2.addGameObject(background);

	scene.addLayer(layer2);
	scene.addLayer(layer);

	console.log("game init");
	Game.init(document.getElementById("gameCanvas"), scene);

	ComponentUtils.addComponent(Game, new MoveTangramComponent().initialize());

	Game.camera.centerPoint.x = 500;
	Game.camera.centerPoint.y = 300;

	Game.camera.translate.setTranslate(0, 0);
	Game.camera.scale.setScale(1, 1);
	Game.camera.rotate.setRotate(0);

}