function MoveTangramComponent() {
}

MoveTangramComponent.prototype = new Component();
MoveTangramComponent.prototype.selectedPiece = null;
MoveTangramComponent.prototype.startX = 0;
MoveTangramComponent.prototype.startY = 0;
MoveTangramComponent.prototype.onMouseDown = function(x, y, wich) {
	var point = MouseSystem.getNormalizedCoordinate(x, y);
	x = point.x;
	y = point.y;

	this.startX = x;
	this.startY = y;

	var selected = Game.scene.listLayers[1].queryGameObjects(x, y, 2, 2, 20);

	this.selectedPiece = selected[0] || null;

	if (this.selectedPiece == null) {
		var go = null;
		if ((x + y) % 2 == 0) {
			go = new BoxObject().initialize(x, y, 0, 40, 40, "white", "black");
		} else {
			go = new CircleObject().initialize(x, y, 0, 20, "white", "black");
		}
		ComponentUtils.addComponent(go, new RigidBodyComponent().initialize(0,
				1, false, false, 0.2));
		Game.scene.listLayers[1].addGameObject(go);
		AssetStore.getAsset("BEEP_AUDIO").getAssetInstance().play();
	} else {
		if (ComponentUtils.getComponent(this.selectedPiece,
				"RIGID_BODY_COMPONENT").density == 0) {
			this.selectedPiece = null;
		}
		if (this.selectedPiece instanceof PolygonObject) {
			ComponentUtils.getComponent(this.selectedPiece,
					"POLYGON_RENDER_COMPONENT").strokeStyle = "white";
		} else if (this.selectedPiece instanceof BoxObject) {
			if (ComponentUtils.getComponent(this.selectedPiece,
					"BOX_RENDER_COMPONENT") != null) {
				ComponentUtils.getComponent(this.selectedPiece,
						"BOX_RENDER_COMPONENT").strokeStyle = "white";
			}
		} else if (this.selectedPiece instanceof CircleObject) {
			ComponentUtils.getComponent(this.selectedPiece,
					"CIRCLE_RENDER_COMPONENT").strokeStyle = "white";
		}
	}
}
MoveTangramComponent.prototype.onMouseUp = function(x, y, wich) {
	this.startX = 0;
	this.startY = 0;
	if (this.selectedPiece != null) {
		if (this.selectedPiece instanceof PolygonObject) {
			ComponentUtils.getComponent(this.selectedPiece,
					"POLYGON_RENDER_COMPONENT").strokeStyle = "black";
		} else if (this.selectedPiece instanceof BoxObject) {
			if (ComponentUtils.getComponent(this.selectedPiece,
					"BOX_RENDER_COMPONENT") != null) {
				ComponentUtils.getComponent(this.selectedPiece,
						"BOX_RENDER_COMPONENT").strokeStyle = "black";
			}
		} else if (this.selectedPiece instanceof CircleObject) {
			ComponentUtils.getComponent(this.selectedPiece,
					"CIRCLE_RENDER_COMPONENT").strokeStyle = "black";
		}
	}
	this.selectedPiece = null;
}
MoveTangramComponent.prototype.onMouseMove = function(x, y, wich) {
	if (this.selectedPiece != null) {
		var point = MouseSystem.getNormalizedCoordinate(x, y);
		x = point.x;
		y = point.y;

		// movimentação
		var dx = (x - this.startX);
		var dy = (y - this.startY);

		this.selectedPiece.addMove(dx, dy);

		this.startX = x;
		this.startY = y;
	}
}
MoveTangramComponent.prototype.onKeyDown = function(keyCode) {
	if (keyCode == 109) {
		if (this.selectedPiece != null) {
			var rotate = ComponentUtils.getComponent(this.selectedPiece,
					"ROTATE_COMPONENT");
			var angle = rotate.getAngle() + (1 / 60);
			rotate.setRotate(angle);
		}
	} else if (keyCode == 107) {
		if (this.selectedPiece != null) {
			var rotate = ComponentUtils.getComponent(this.selectedPiece,
					"ROTATE_COMPONENT");
			var angle = rotate.getAngle() - (1 / 60);
			rotate.setRotate(angle);
		}
	} else if (keyCode == 71) {
		if (Game.scene.listLayers[1].gravity == 0) {
			Game.scene.listLayers[1].setGravity(1000);
		} else {
			Game.scene.listLayers[1].setGravity(0);
		}
	}
}
MoveTangramComponent.prototype.getSystems = function() {
	var systems = new Array();
	systems = ArrayUtils.addElement(systems, KeySystem);
	systems = ArrayUtils.addElement(systems, MouseSystem);
	return systems;
}
MoveTangramComponent.prototype.getTag = function() {
	return "MOVE_TANGRAM_COMPONENT";
}

/*
 * *****************************************************************************************
 */

var MoveTangramComponentParser = function() {
	this.parse = function(object) {
		var data = {
			className : "MoveTangramComponent",
			selectedPiece : object.selectedPiece,
			startX : object.startX,
			startY : object.startY,
		}

		return data;
	};

	this.unparse = function(data) {
		var obj = new MoveTangramComponent();
		obj.selectedPiece = data.selectedPiece;
		obj.startX = data.startX;
		obj.startY = data.startY;
		return obj;
	};

};

MoveTangramComponent.prototype._className = "MoveTangramComponent";
MetadataManager.addParser("MoveTangramComponent", MoveTangramComponentParser);