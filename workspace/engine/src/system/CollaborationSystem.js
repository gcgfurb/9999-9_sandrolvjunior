
var CollaborationSystem = new function()
{
	this.fireJoinListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onJoin", [evt]);
	};
	
	this.fireSynchListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onSynch", [evt]);
	};
	
	this.fireDrawBoardListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onDrawBoard", [evt]);
	}

	this.fireClearBoardListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onClearBoard", [evt]);
	}
	
	this.getTag = function(){
		return "COLLABORATION_SYSTEM";
	};

	this.getListName = function(){
		return "listComponentsCollaboration";
	};
}