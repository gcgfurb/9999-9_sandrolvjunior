//TODO:Documentação

function CollaborationComponent()
{
}

CollaborationComponent.prototype = new Component();

JSUtils.addMethod(CollaborationComponent.prototype, "initialize", function(collaborationStrategy, boxDiv)
{
	this.initialize();

	this.collaborationStrategy = collaborationStrategy;

	this.fireEventFunction = ComponentUtils.fireComponentEvent;

	this.boardMode = false;

	this.ignoredSystems =
		[
				LogicSystem.getListName(), CollaborationSystem.getListName()
		];

	this.lastMouse =
		{
			x : 0,
			y : 0
		};

	
	this.setUp(boxDiv);
	
	this.isMaster = true;

	return this;
});

CollaborationComponent.prototype.setUp = function(boxDiv)
{
	this.collaborationStrategy.setUp(this);
	this.createInterruption();

	var width = parseInt(boxDiv.style.width, 10);
	var height = parseInt(boxDiv.style.height, 10);

	var gameCanvas = boxDiv.firstElementChild;

	gameCanvas.width = width;
	gameCanvas.height = height;
	gameCanvas.style.position = "absolute";
	gameCanvas.style.left = 0;
	gameCanvas.style.top = 0;

	var boardCanvas = document.createElement("canvas");
	boxDiv.appendChild(boardCanvas);

	boardCanvas.id = "boardCanvas";
	boardCanvas.width = width;
	boardCanvas.height = height;
	boardCanvas.style.position = "absolute";
	boardCanvas.style.left = 0;
	boardCanvas.style.top = 0;
	
	this.sketchBoard = new SketchBoard(boardCanvas, this.collaborationStrategy);
};

CollaborationComponent.prototype.createInterruption = function()
{
	var self = this;

	ComponentUtils.fireComponentEvent = function(listName, functionStr, paramsArray)
	{
		if (listName === MouseSystem.getListName())
		{
			if (self.sketchBoard.preventEvent(functionStr, paramsArray))
				return;
		}

		self.fireEventFunction.apply(ComponentUtils, arguments);

		if (ArrayUtils.contains(self.ignoredSystems, listName))
		{
			return;
		}

		//self.collaborationStrategy.sendEvent(listName, functionStr, paramsArray);
	};
};

/*
CollaborationComponent.prototype.onSystemEvent = function(event)
{
	this.fireEventFunction.call(ComponentUtils, event.listName, event.functionStr, event.paramsArray);
};
*/

CollaborationComponent.prototype.onJoin = function(event)
{
	this.isMaster = false;
};

CollaborationComponent.prototype.onSynch = function(event)
{
	//TODO: receber o game state e setá-lo no jogo
};

CollaborationComponent.prototype.onDrawBoard = function(event)
{
	this.sketchBoard.draw(event.start, event.end, event.color, event.size, event.compositeOperation, true);
};

CollaborationComponent.prototype.onClearBoard = function(event)
{
	this.sketchBoard.clear(false);
};

CollaborationComponent.prototype.changeBoardMode = function()
{
	this.boardMode = !this.boardMode;
};

CollaborationComponent.prototype.getSystems = function()
{
	var systems = new Array();
	systems = ArrayUtils.addElement(systems, CollaborationSystem);
	return systems;
};

CollaborationComponent.prototype.getTag = function()
{
	return "COLLABORATION_COMPONENT";
};