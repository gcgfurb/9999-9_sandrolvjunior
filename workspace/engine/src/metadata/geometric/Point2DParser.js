var Point2DParser = function() {
};
Point2DParser.prototype.parse = function(object) {
	var data = {
		className : "Point2D",
		x : object.x,
		y : object.y,
	}

	return data;
};

Point2DParser.prototype.unparse = function(data) {

	return new Point2D().initialize(data.x, data.y);
};

Point2D.prototype._className = "Point2D";
MetadataManager.addParser("Point2D", Point2DParser);