var Point3DParser = function() {
};

Point3DParser.prototype.parse = function(object) {
	var data = {
		className : "Point3D",
		x : object.x,
		y : object.y,
		z : object.z,
	}

	return data;
};

Point3DParser.prototype.unparse = function(data) {

	return new Point3D().initialize(data.x, data.y, data.z);
};

Point3D.prototype._className = "Point3D";
MetadataManager.addParser("Point3D", Point3DParser);