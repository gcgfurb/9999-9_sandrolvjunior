var LayerParser = function() {
};
LayerParser.prototype.parse = function(object) {
	var data = {
		className : "Layer",
		listComponents : MetadataManager.parseList(object.listComponents),
		listGameObjects : MetadataManager.parseList(object.listGameObjects),
		gravity : object.gravity,
		doSleep : object.doSleep,
	}

	return data;
};

LayerParser.prototype.unparse = function(data) {

	var layer = new Layer().initialize();
	layer.gravity = data.gravity;
	layer.doSleep = data.doSleep;

	MetadataManager.clearComponents(layer);
	MetadataManager.unparseComponentList(layer, MetadataManager
			.unparseList(data.listComponents));

	return layer;
};

Layer.prototype._className = "Layer";
MetadataManager.addParser("Layer", LayerParser);