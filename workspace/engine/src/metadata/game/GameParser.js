var GameParser = function() {
};

GameParser.prototype.parse = function(object) {
	var data = {
		className : "Game",
		canvasId : Game.canvasId,
		camera : MetadataManager.parse(Game.camera),
		scene : MetadataManager.parse(Game.scene),
		listComponents : MetadataManager.parseList(Game.listComponents),
		// listAssets : MetadataManager.parseList(Game.listAssets),
		running : Game.running,
		paused : Game.paused,
		apiName : Game.apiName
	}

	return data;
};

GameParser.prototype.unparse = function(data) {
	Game.loadAPI(data.apiName);

	var scene = MetadataManager.unparse(data.camera);
	Game.init(document.getElementById(data.canvasId), scene);

	Game.camera = MetadataManager.unparse(data.camera);

	MetadataManager.clearComponents(Game);
	MetadataManager.unparseComponentList(Game, MetadataManager
			.unparseList(data.listComponents));

	// Game.listAssets = MetadataManager.unparseList(data.listAssets);

	Game.running = data.running;
	Game.paused = data.paused;

	return Game;
};

Game._className = "Game";
MetadataManager.addParser("Game", GameParser);