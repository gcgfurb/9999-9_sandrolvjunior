var CameraParser = function() {
};
CameraParser.prototype.parse = function(object) {
	var data = {
		className : "Camera",
		listComponents : MetadataManager.parseList(object.listComponents),
		centerPoint : MetadataManager.parse(object.centerPoint),
		translate : MetadataManager.parse(object.translate),
		scale : MetadataManager.parse(object.scale),
		rotate : MetadataManager.parse(object.rotate),
	}

	return data;
};

this.unparse = function(data) {

	var centerPoint = MetadataManager.unparse(data.centerPoint);

	Game.camera.initialize(centerPoint.x, centerPoint.y, centerPoint.z, 0, 0,
			0, 0, 0);

	Game.camera.translate = MetadataManager.unparse(data.translate);
	Game.camera.scale = MetadataManager.unparse(data.scale);
	Game.camera.rotate = MetadataManager.unparse(data.rotate);

	MetadataManager.clearComponents(Game.camera);
	MetadataManager.unparseComponentList(Game.camera, MetadataManager
			.unparseList(data.listComponents));

	return Game.camera;
};

Camera.prototype._className = "Camera";
MetadataManager.addParser("Camera", CameraParser);