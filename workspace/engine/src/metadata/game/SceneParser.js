var SceneParser = function() {
};
SceneParser.prototype.parse = function(object) {
	var data = {
		className : "Scene",
		listComponents : MetadataManager.parseList(object.listComponents),
		listLayers : MetadataManager.parseList(object.listLayers),
		minX : minPoint.x,
		minY : minPoint.y,
		maxX : maxPoint.x,
		maxY : maxPoint.y
	}

	return data;
};

SceneParser.prototype.unparse = function(data) {
	var scene = new Scene().initialize(data.minX, data.minY, data.maxX,
			data.maxY);

	MetadataManager.clearComponents(scene);
	MetadataManager.unparseComponentList(scene, MetadataManager
			.unparseList(data.listComponents));

	layers = MetadataManager.unparseList(data.listLayers);

	delete scene.listLayer;
	scene.listLayer = new Array();

	for ( var i in layers) {
		scene.addLayer(layers[i]);
	}

	return scene;
};

Scene.prototype._className = "Scene";
MetadataManager.addParser("Scene", SceneParser);