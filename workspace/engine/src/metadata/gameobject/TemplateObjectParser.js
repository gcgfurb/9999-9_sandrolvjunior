var TemplateObjectParser = function() {
};
TemplateObjectParser.prototype.parse = function(object) {
	var data = {
		className : "TemplateObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		width : object.width,
		height : object.height,
		depth : object.depth,
		component : MetadataManager.parse(object.component),
		tag : object.tag
	}

	return data;
};

TemplateObjectParser.prototype.unparse = function(data) {
	var obj = new TemplateObject().initialize(data.x, data.y, data.z,
			data.width, data.height, data.depth, MetadataManager
					.unparse(data.component), data.tag);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

TemplateObject.prototype._className = "TemplateObject";
MetadataManager.addParser("TemplateObject", TemplateObjectParser);