var BoxObjectParser = function() {
};
BoxObjectParser.prototype.parse = function(object) {
	var data = {
		className : "BoxObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		width : object.width,
		height : object.height
	}

	var renderComponenet = ComponentUtils.getComponent(object,
			"BOX_RENDER_COMPONENT");

	data.fillStyle = renderComponenet.fillStyle;
	data.fillStroke = renderComponenet.strokeStyle;

	return data;
};

BoxObjectParser.prototype.unparse = function(data) {
	var obj = new BoxObject().initialize(data.x, data.y, data.z, data.width,
			data.height, data.fillStyle, data.fillStroke);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

BoxObject.prototype._className = "BoxObject";
MetadataManager.addParser("BoxObject", BoxObjectParser);