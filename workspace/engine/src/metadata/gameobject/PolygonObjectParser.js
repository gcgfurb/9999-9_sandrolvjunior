var PolygonObjectParser = function() {
};
PolygonObjectParser.prototype.parse = function(object) {
	var data = {
		className : "PolygonObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		points : MetadataManager.parseList(object.points),
		faces : MetadataManager.parseList(object.faces),
	}

	var renderComponenet = ComponentUtils.getComponent(object,
			"POLYGON_RENDER_COMPONENT");

	data.fillStyle = renderComponenet.fillStyle;
	data.fillStroke = renderComponenet.strokeStyle;

	return data;
};

PolygonObjectParser.prototype.unparse = function(data) {

	var obj = new PolygonObject().initialize(data.x, data.y, data.z,
			data.points, data.faces, data.fillStyle, data.fillStroke);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

PolygonObject.prototype._className = "PolygonObject";
MetadataManager.addParser("PolygonObject", PolygonObjectParser);