var TemplateWithPointsObjectParser = function() {
};
TemplateWithPointsObjectParser.prototype.parse = function(object) {
	var data = {
		className : "TemplateWithPointsObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		width : object.width,
		height : object.height,
		depth : object.depth,
		points : MetadataManager.parseList(object.points),
		component : MetadataManager.parse(object.component),
		tag : object.tag
	}

	return data;
};

TemplateWithPointsObjectParser.prototype.unparse = function(data) {
	var obj = new TemplateWithPointsObject().initialize(data.x, data.y, data.z,
			data.width, data.height, data.depth, MetadataManager
					.unparseList(data.points), MetadataManager
					.unparse(data.component), data.tag);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

TemplateWithPointsObject.prototype._className = "TemplateWithPointsObject";
MetadataManager.addParser("TemplateWithPointsObject",
		TemplateWithPointsObjectParser);