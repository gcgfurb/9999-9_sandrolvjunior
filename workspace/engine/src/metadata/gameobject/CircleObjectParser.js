var CircleObjectParser = function() {
};
CircleObjectParser.prototype.parse = function(object) {
	var data = {
		className : "CircleObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		radius : object.radius,
	}

	var renderComponenet = ComponentUtils.getComponent(object,
			"CIRCLE_RENDER_COMPONENT");

	data.fillStyle = renderComponenet.fillStyle;
	data.fillStroke = renderComponenet.strokeStyle;

	return data;
};

CircleObjectParser.prototype.unparse = function(data) {
	var obj = new CircleObject().initialize(data.x, data.y, data.z,
			data.radius, data.fillStyle, data.fillStroke);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

CircleObject.prototype._className = "CircleObject";
MetadataManager.addParser("CircleObject", CircleObjectParser);