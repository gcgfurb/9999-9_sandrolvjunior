var GameObjectParser = function() {
};
GameObjectParser.prototype.parse = function(object) {
	var data = {
		className : "GameObject",
		listComponents : MetadataManager.parseList(object.listComponents),
		x : object.origin.x,
		y : object.origin.y,
		z : object.origin.z,
		width : object.width,
		height : object.height,
		depth : object.depth
	}

	return data;
};

GameObjectParser.prototype.unparse = function(data) {

	var obj = new GameObject().initialize(data.x, data.y, data.z, data.width,
			data.height, data.epth);

	MetadataManager.clearComponents(obj);
	MetadataManager.unparseComponentList(obj, MetadataManager
			.unparseList(data.listComponents));

	return obj;
};

GameObject.prototype._className = "GameObject";
MetadataManager.addParser("GameObject", GameObjectParser);