var MetadataManager = new function() {

	this.parsers = {};
	this.systemListNames = [ "listComponentsCollaboration",
			"listComponentsGamepad", "listComponentsKey",
			"listComponentsKinect", "listComponentsLogic",
			"listComponentsMouse", "listComponentsRender",
			"listComponentsTouch" ];

	this.addParser = function(className, parser) {
		MetadataManager.parsers[className] = new parser();
	};

	this.parse = function(obj) {
		
		if (!obj)
			return null;
		
		var className = JSUtils.getClassName(obj);
		// console.log(className);
		return MetadataManager.parsers[className].parse(obj);
	};

	this.unparse = function(data) {

		if (!data)
			return null;
		
		return MetadataManager.parsers[data.className].unparse(data);
	};

	this.parseList = function(objList) {

		console.log(objList);
		console.log(Array.isArray(objList));

		if (!objList)
			return null;

		var dataList = {};

		if (Array.isArray(objList)) {
			for (var i = 0; i < objList.length; i++) {
				dataList[i] = MetadataManager.parse(objList[i]);
			}
		} else {
			for ( var j in objList) {
				dataList[j] = MetadataManager.parse(objList[j]);
			}
		}

		return dataList;
	};

	this.unparseList = function(dataList) {

		if (!dataList)
			return null;

		var objList = {};

		for ( var i in dataList) {
			objList[i] = MetadataManager.unparse(dataList[i]);
		}

		return objList;
	};

	this.unparseComponentList = function(parent, components) {
		for ( var i in components) {
			ComponentUtils.addComponent(parent, components[i]);
		}
	};

	this.clearComponents = function(parent) {
		for (var i = 0; i < systemListNames.length; i++) {
			var listName = systemListNames[i];

			if (parent[listName]) {
				delete parent[listName];
				parent[listName] = null;
			}
		}
	};

}