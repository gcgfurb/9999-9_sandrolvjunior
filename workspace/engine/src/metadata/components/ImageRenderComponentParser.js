var ImageRenderComponentParser = function() {};
	ImageRenderComponentParser.prototype.parse = function(object) {
		var data = {
			className : "ImageRenderComponent",
			assetId : object.image.id,
			repeat : object.repeat,
			direction : object.direction
		}

		return data;
	};

	ImageRenderComponentParser.prototype.unparse = function(data) {

		return new ImageRenderComponent().initialize(AssetStore.getAsset(data.assetId)
				.getAssetInstance(), data.repeat, data.direction);
	};

ImageRenderComponent.prototype._className = "ImageRenderComponent";
MetadataManager.addParser("ImageRenderComponent", ImageRenderComponentParser);