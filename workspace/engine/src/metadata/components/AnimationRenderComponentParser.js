var AnimationRenderComponentParser = function() {
};
AnimationRenderComponentParser.prototype.parse = function(object) {
	var data = {
		className : "AnimationRenderComponent",
		spriteSheetId : object.spriteSheet.id,
		columns : object.columns,
		rows : object.rows,
		uv : MetadataManager.parseList(object.uv),
		stopped : object.stopped,
		direction : object.direction,
		animation : object.animation,
		start : object.start,
		end : object.end,
		interactions : object.interactions,
		duration : object.duration,
		lastUpdate : object.lastUpdate,
		currentInteraction : object.currentInteraction,
		currentFrame : object.currentFrame,
	}

	return data;
};

AnimationRenderComponentParser.prototype.unparse = function(data) {

	var obj = new AnimationRenderComponent().initialize(AssetStore.getAsset(
			data.spriteSheetId).getAssetInstance(), data.columns, data.rows);

	obj.uv = data.uv;
	obj.stopped = data.stopped;
	obj.direction = data.direction;
	obj.animation = data.animation;
	obj.start = data.start;
	obj.end = data.end;
	obj.interactions = data.interactions;
	obj.duration = data.duration;
	obj.lastUpdate = data.lastUpdate;
	obj.currentInteraction = data.currentInteraction;
	obj.currentFrame = data.currentFrame;

	return obj;
};

AnimationRenderComponent.prototype._className = "AnimationRenderComponent";
MetadataManager.addParser("AnimationRenderComponent",
		AnimationRenderComponentParser);