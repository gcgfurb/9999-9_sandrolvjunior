var ComponentParser = function() {
};
ComponentParser.prototype.parse = function(object) {
	var data = {
		className : "Component",
	}

	return data;
};

ComponentParser.prototype.unparse = function(data) {

	return new Component();
};

Component.prototype._className = "Component";
MetadataManager.addParser("Component", ComponentParser);