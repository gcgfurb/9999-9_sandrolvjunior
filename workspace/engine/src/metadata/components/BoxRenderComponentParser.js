var BoxRenderComponentParser = function() {};
BoxRenderComponentParser.prototype.parse = function(object) {
		var data = {
			className : "BoxRenderComponent",
			fillStyle : object.fillStyle,
			strokeStyle : object.strokeStyle,
			texture : object.texture,
		}

		return data;
	};

	BoxRenderComponentParser.prototype.unparse = function(data) {
		return new BoxRenderComponent().initialize(data.fillStyle, data.strokeStyle, data.texture);
	};

BoxRenderComponent.prototype._className = "BoxRenderComponent";
MetadataManager.addParser("BoxRenderComponent", BoxRenderComponentParser);