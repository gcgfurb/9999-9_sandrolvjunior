var FpsMeterComponentParser = function() {
};
FpsMeterComponentParser.prototype.parse = function(object) {
	var data = {
		className : "FpsMeterComponent",
	}

	return data;
};

FpsMeterComponentParser.prototype.unparse = function(data) {

	return new FpsMeterComponent();

};

FpsMeterComponent.prototype._className = "FpsMeterComponent";
MetadataManager.addParser("FpsMeterComponent", FpsMeterComponentParser);