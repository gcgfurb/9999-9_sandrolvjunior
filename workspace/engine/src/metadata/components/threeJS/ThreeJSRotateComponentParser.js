var ThreeJSRotateComponentParser = function() {
};
ThreeJSRotateComponentParser.prototype.parse = function(object) {
	var data = {
		className : "ThreeJSRotateComponent",
		x : object.angle.x,
		y : object.angle.y,
		z : object.angle.z
	}

	return data;
};

ThreeJSRotateComponentParser.prototype.unparse = function(data) {
	return new ThreeJSRotateComponent().initialize(new THREE.Vector3(data.x,
			data.y, data.z));
};

ThreeJSRotateComponent.prototype._className = "ThreeJSRotateComponent";
MetadataManager.addParser("ThreeJSRotateComponent",
		ThreeJSRotateComponentParser);