var ThreeJSScaleComponentParser = function() {
};
ThreeJSScaleComponentParser.prototype.parse = function(object) {
	var data = {
		className : "ThreeJSScaleComponent",
		x : object.scalePoint.x,
		y : object.scalePoint.y,
		z : object.scalePoint.z
	}

	return data;
};

ThreeJSScaleComponentParser.prototype.unparse = function(data) {
	return new ThreeJSScaleComponent().initialize(new THREE.Vector3(data.x,
			data.y, data.z));
};

ThreeJSScaleComponent.prototype._className = "ThreeJSScaleComponent";
MetadataManager.addParser("ThreeJSScaleComponent", ThreeJSScaleComponentParser);