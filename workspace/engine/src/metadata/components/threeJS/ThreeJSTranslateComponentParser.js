var ThreeJSTranslateComponentParser = function() {
};
ThreeJSTranslateComponentParser.prototype.parse = function(object) {
	var data = {
		className : "ThreeJSTranslateComponent",
		x : object.translatePoint.x,
		y : object.translatePoint.y,
		z : object.translatePoint.z
	}

	return data;
};

ThreeJSTranslateComponentParser.prototype.unparse = function(data) {
	return new ThreeJSTranslateComponent().initialize(new THREE.Vector3(data.x,
			data.y, data.z));
};

ThreeJSTranslateComponent.prototype._className = "ThreeJSTranslateComponent";
MetadataManager.addParser("ThreeJSTranslateComponent",
		ThreeJSTranslateComponentParser);