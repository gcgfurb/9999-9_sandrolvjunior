var CanvasRotateComponentParser = function() {
};
CanvasRotateComponentParser.prototype.parse = function(object) {
	var data = {
		className : "CanvasRotateComponent",
		angle : object.angle,
	}

	return data;
};

CanvasRotateComponentParser.prototype.unparse = function(data) {

	return new CanvasRotateComponent().initialize(data.angle);
};

CanvasRotateComponent.prototype._className = "CanvasRotateComponent";
MetadataManager.addParser("CanvasRotateComponent", CanvasRotateComponentParser);