var CanvasScaleComponentParser = function() {
};
CanvasScaleComponentParser.prototype.parse = function(object) {
	var data = {
		className : "CanvasScaleComponent",
		x : object.x,
		y : object.y
	}

	return data;
};

CanvasScaleComponentParser.prototype.unparse = function(data) {
	return new CanvasScaleComponent().initialize(data.x, data.y);
};

CanvasScaleComponent.prototype._className = "CanvasScaleComponent";
MetadataManager.addParser("CanvasScaleComponent", CanvasScaleComponentParser);