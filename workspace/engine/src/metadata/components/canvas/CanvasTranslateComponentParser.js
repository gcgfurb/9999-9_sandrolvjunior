var CanvasTranslateComponentParser = function() {};
CanvasTranslateComponentParser.prototype.parse = function(object) {
		var data = {
			className : "CanvasTranslateComponent",
			x : object.x,
			y : object.y
		}

		return data;
	};

	CanvasTranslateComponentParser.prototype.unparse = function(data) {
		return new CanvasTranslateComponent().initialize(data.x, data.y);
	};

CanvasTranslateComponent.prototype._className = "CanvasTranslateComponent";
MetadataManager.addParser("CanvasTranslateComponent", CanvasTranslateComponentParser);