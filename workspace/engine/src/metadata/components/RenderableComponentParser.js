var RenderableComponentParser = function() {};
RenderableComponentParser.prototype.parse = function(object) {
		var data = {
			className : "RenderableComponent",
		}

		return data;
	};

	RenderableComponentParser.prototype.unparse = function(data) {

		return new RenderableComponent();
	};

RenderableComponent.prototype._className = "RenderableComponent";
MetadataManager.addParser("RenderableComponent", RenderableComponentParser);