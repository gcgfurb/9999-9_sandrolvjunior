var MoveCameraComponentParser = function() {
};
MoveCameraComponentParser.prototype.parse = function(object) {
	var data = {
		className : "MoveCameraComponent",
		leftKey : object.leftKey,
		rightKey : object.rightKey,
		upKey : object.upKey,
		downKey : object.downKey,
		zoomInKey : object.zoomInKey,
		zoomOutKey : object.zoomOutKey,
		rotateKey : object.rotateKey,
		rotateInverseKey : object.rotateInverseKey,
		translateFactor : object.translateFactor,
		scaleFactor : object.scaleFactor,
		rotateFactor : object.rotateFactor
	}

	return data;
};

MoveCameraComponentParser.prototype.unparse = function(data) {

	return new MoveCameraComponent().initialize(data.leftKey, data.rightKey,
			data.upKey, data.downKey, data.zoomInKey, data.zoomOutKey,
			data.rotateKey, data.rotateInverseKey, data.translateFactor,
			data.scaleFactor, data.rotateFactor);
};

MoveCameraComponent.prototype._className = "MoveCameraComponent";
MetadataManager.addParser("MoveCameraComponent", MoveCameraComponentParser);