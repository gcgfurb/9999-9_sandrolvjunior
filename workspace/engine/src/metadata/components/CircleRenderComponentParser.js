var CircleRenderComponentParser = function() {
};
CircleRenderComponentParser.prototype.parse = function(object) {
	var data = {
		className : "CircleRenderComponent",
		fillStyle : object.fillStyle,
		radius : object.radius,
	}

	return data;
};

CircleRenderComponentParser.prototype.unparse = function(data) {
	return new CircleRenderComponent().initialize(data.fillStyle, data.radius);
};

CircleRenderComponent.prototype._className = "CircleRenderComponent";
MetadataManager.addParser("CircleRenderComponent", CircleRenderComponentParser);