var RigidBodyComponentParser = function() {
};
RigidBodyComponentParser.prototype.parse = function(object) {
	var data = {
		className : "RigidBodyComponent",
		restitution : object.restitution,
		density : object.density,
		preventRotation : object.preventRotation,
		allowSleep : object.allowSleep,
		friction : object.friction,
		isSensor : object.isSensor,
		isCollidable : object.isCollidable
	}

	return data;
};

RigidBodyComponentParser.prototype.unparse = function(data) {

	return new RigidBodyComponent().initialize(data.restitution, data.density,
			data.preventRotation, data.allowSleep, data.friction,
			data.isSensor, data.isCollidable);
};

RigidBodyComponent.prototype._className = "RigidBodyComponent";
MetadataManager.addParser("RigidBodyComponent", RigidBodyComponentParser);