var PolygonRenderComponentParser = function() {
};
PolygonRenderComponentParser.prototype.parse = function(object) {
	var data = {
		className : "PolygonRenderComponent",
		fillStyle : object.fillStyle,
		strokeStyle : object.strokeStyle
	}

	return data;
};

PolygonRenderComponentParser.prototype.unparse = function(data) {

	var obj = new PolygonRenderComponent().initialize(data.fillStyle,
			data.fillStroke);

	return obj;
};

PolygonRenderComponent.prototype._className = "PolygonRenderComponent";
MetadataManager.addParser("PolygonRenderComponent",
		PolygonRenderComponentParser);