function TogetherJSUI(strategy, component)
{
	this.strategy = strategy;
	this.component = component;
}

TogetherJSUI.prototype.setUp = function()
{
	this.createCanvasButton();
	this.createCanvasToolBar();
};

TogetherJSUI.prototype.addButton = function(button)
{
	document.getElementById("togetherjs-buttons").insertBefore(button, document.getElementById("togetherjs-chat-button"));
	
	var dock = document.getElementById("togetherjs-dock");
	dock.style.height = (parseInt(dock.style.height, 10) + 61) + "px";
};

TogetherJSUI.prototype.createCanvasButton = function()
{
	var self = this;
	this.boardButton = document.createElement("button");
	this.boardButton.id = "visedu-board-button";
	this.boardButton.className = "togetherjs-button";
	
	this.boardButton.onclick = function()
	{
		var active = self.component.sketchBoard.toogleActive();
		
		this.boardToolBarDiv.style.dysplay = active ? "initial" : "none";
	};

	this.addButton(this.boardButton);

}

TogetherJSUI.prototype.createCanvasToolBar = function()
{
	var self = this;
	
	this.boardToolBarDiv = document.createElement("div");
	this.boardToolBarDiv.id = "visedu-board-toolbar-outter";
	
	this.boardToolBarDiv.style.dysplay = "none";
	this.boardToolBarDiv.style.position = "fixed";
	this.boardToolBarDiv.style.bottom = "0px";
	this.boardToolBarDiv.style.width = "100%";
	
	document.body.appendChild(this.boardToolBarDiv);
	
	var toolBarInner = document.createElement("div");
	toolBarInner.id = "visedu-board-toolbar-inner";
	
	toolBarInner.style.margin = "auto";
	toolBarInner.style.width = "80%";
	
	boardToolBarDiv.appendChild(toolBarInner);
	

	var buttonBlack = document.createElement("button");
	buttonBlack.className = "togetherjs-button";
	
	buttonBlack.onclick = function()
	{
		self.component.sketchBoard.setColor("#000");
	};
	
	toolBarInner.appendChild(buttonBlack);
	
}