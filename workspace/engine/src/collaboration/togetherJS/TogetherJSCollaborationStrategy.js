//TODO: Documentação
//Implementação da strategy de colaboração usando a API TogetherJS

function TogetherJSCollaborationStrategy()
{
}

TogetherJSCollaborationStrategy.prototype = new CollaborationStrategy();

TogetherJSCollaborationStrategy.prototype.setUp = function(component)
{
	TogetherJSConfig_dontShowClicks = true;
	TogetherJSConfig_disableWebRTC = true;
	TogetherJSConfig_cloneClicks = false;
	TogetherJSConfig_suppressJoinConfirmation = true;
	TogetherJSConfig_ignoreMessages = true;

	//TogetherJS.hub.on("systemEvent", CollaborationSystem.fireSystemEventListener);

	TogetherJS.hub.on("synch", CollaborationSystem.fireSynchListener);

	TogetherJS.hub.on("drawBoard", CollaborationSystem.fireDrawBoardListener);

	TogetherJS.hub.on("clearBoard", CollaborationSystem.fireClearBoardListener);

	TogetherJS.hub.on("togetherjs.hello", CollaborationSystem.fireJoinListener);

	TogetherJS(this);
	
	this.ui = new TogetherJSUI(this, component);

	var self = this;
	TogetherJS.once("ready", function()
	{
		self.ui.setUp();
	});
};
/*
TogetherJSCollaborationStrategy.prototype.sendEvent = function(listName, functionStr, paramsArray)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "systemEvent",
				listName : listName,
				functionStr : functionStr,
				paramsArray : paramsArray
			});
	}
};
*/

TogetherJSCollaborationStrategy.prototype.sendGameData = function(data)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "synch",
				data : data
			});
	}
};

TogetherJSCollaborationStrategy.prototype.drawBoard = function(start, end, color, size, compositeOperation)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "drawBoard",
				start : start,
				end : end,
				color : color,
				size : size,
				compositeOperation : compositeOperation
			});
	}
};

TogetherJSCollaborationStrategy.prototype.clearBoard = function()
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "clearBoard",
			});
	}
};