function SketchBoard(canvas, collabStrategy)
{
	this.active = false;
	this.drawing = false;

	this.canvas = canvas;

	this.collabStrategy = collabStrategy;

	this.context = this.canvas.getContext('2d');
	this.context.lineWidth = 2;
	this.context.lineJoin = 'round';
	this.context.lineCap = 'round';
	this.context.strokeStyle = '#000';

	this.lines = [];

	this.lastMouse =
		{
			x : 0,
			y : 0
		};
}

SketchBoard.prototype.toogleActive = function()
{
	this.active = !this.active;
	return this.active;
};

SketchBoard.prototype.preventEvent = function(functionStr, paramsArray)
{
	if (!this.active)
		return false;
	
	if (functionStr === "onMouseDown")
	{
		this.onMouseDown(paramsArray[0], paramsArray[1]);
		return true;
	}

	if (functionStr === "onMouseUp")
	{
		this.onMouseUp(paramsArray[0], paramsArray[1]);
		return true;
	}

	if (functionStr === "onMouseMove")
	{
		this.onMouseMove(paramsArray[0], paramsArray[1]);
		return true;
	}
	
	return false;
};

SketchBoard.prototype.onMouseDown = function(x, y)
{
	this.lastMouse =
		{
			x : x,
			y : y
		};

	this.drawing = true;
};

SketchBoard.prototype.onMouseUp = function(x, y)
{
	this.drawing = false;
};

SketchBoard.prototype.onMouseMove = function(x, y)
{
	if (!this.drawing)
		return;

	var mouse =
		{
			x : x,
			y : y
		};

	this.draw(this.lastMouse, mouse, this.context.strokeStyle, this.context.lineWidth, this.context.globalCompositeOperation, true);
	this.collabStrategy.drawBoard(this.lastMouse, mouse, this.context.strokeStyle, this.context.lineWidth, this.context.globalCompositeOperation);

	this.lastMouse = mouse;
};

SketchBoard.prototype.increaseSize = function()
{
	this.context.lineWidth += 5;
};

SketchBoard.prototype.decreaseSize = function()
{
	if (this.context.lineWidth > 5)
		this.context.lineWidth -= 5;
};

SketchBoard.prototype.setColor = function(color)
{
	this.context.globalCompositeOperation = 'source-over';
	this.context.strokeStyle = color;
};

SketchBoard.prototype.setEraser = function()
{
	this.context.globalCompositeOperation = 'destination-out';
	this.context.strokeStyle = 'rgba(0,0,0,1)';
};

SketchBoard.prototype.clear = function(send)
{
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	console.log(this.canvas.width);
	console.log(this.canvas.height);
	this.lines = [];
	if (send)
		this.collabStrategy.clearBoard();
};

// Redraws the lines from the lines-array:
SketchBoard.prototype.reDraw = function(lines)
{
	for ( var i in lines)
	{
		this.draw(lines[i][0], lines[i][1], lines[i][2], lines[i][3], lines[i][4], false);
	}
};

SketchBoard.prototype.draw = function(start, end, color, size, compositeOperation, save)
{
	this.context.save();
	this.context.lineJoin = 'round';
	this.context.lineCap = 'round';
	// Since the coordinates have been translated to an 1140x400 canvas, the context needs to be scaled before it can be drawn on:
	//this.context.scale(canvas.width / 1140, canvas.height / 400);
	this.context.strokeStyle = color;
	this.context.globalCompositeOperation = compositeOperation;
	this.context.lineWidth = size;
	this.context.beginPath();
	this.context.moveTo(start.x, start.y);
	this.context.lineTo(end.x, end.y);
	this.context.closePath();
	this.context.stroke();
	this.context.restore();
	if (save)
	{
		// Won't save if draw() is called from reDraw().
		this.lines.push(
			[
						{
							x : start.x,
							y : start.y
						},
						{
							x : end.x,
							y : end.y
						}, color, size, compositeOperation
			]);
	}
};
