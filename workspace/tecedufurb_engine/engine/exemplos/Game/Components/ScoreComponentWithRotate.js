/**
 * Created by Gabriel on 16/11/2015.
 */

function ScoreComponent(){}

ScoreComponent.prototype = new Component();

ScoreComponent.prototype.scores = 0;
ScoreComponent.prototype.scoreWins = 0;
ScoreComponent.prototype.scoreLoses = 0;
ScoreComponent.prototype.rotate = 0;
ScoreComponent.prototype.position = [0,0];


JSUtils.addMethod(ScoreComponent.prototype, "initialize",
    function(scoreWins, scoreLoses, rotate, position){
        this.initialize();
        this.scoreWins = scoreWins;
        this.scoreLoses = scoreLoses;
        this.rotate = rotate;
        this.position = position;
        return this;
    }
);

ScoreComponent.prototype.onWin = function(){
    this.scores += this.scoreWins;
};

ScoreComponent.prototype.onLose = function(){
    this.scores = (this.scores -= this.scoreLoses) > 0 ? this.scores : 0;
};

ScoreComponent.prototype.onRender = function(context){
   /* context.fillStyle = "red";
    context.font = "bold 20px Arial";
    /*Precisa ser feito a translaçãp primeiro*/
    /*context.translate(this.position[0],this.position[1]);
    context.rotate(this.rotate*Math.PI/180);
    context.fillText("Pontos: "+this.scores, 120, 100);
    //context.restore();*/
    context.fillRect(this.position[0], this.position[1], 100, 100);
    //console.log(this.position[0]);
};

ScoreComponent.prototype.getSystems = function(){
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, RenderSystem);
    return systems;
};

ScoreComponent.prototype.getTag = function(){
    return "SCORE_COMPONENT_ROTATE";
};