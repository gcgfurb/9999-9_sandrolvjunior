/*
	Classe que representa a entidade 'Eixo' (x e y) na cena
	
	Descricao: Eixos de orientacao X e Y no plano cartesiano
*/
var Eixos3D = function(){
	var eixosX, eixosY, eixosZ;
    var geoX = [];
	var geoY = [];
	var geoZ = [];

    //Criar um Layer para adicionar cada eixo separadamente.
    var layer = new Layer().initialize();

    geoX.push(new THREE.Vector3(), new THREE.Vector3( -17, 0, 0 ));
    eixosX = ThreeJSBuilder.createLinesObject(0,0,0,geoX,0xFF000F);
    eixosX.threeObject.material = new THREE.LineDashedMaterial( { color: 0xff0000, dashSize: 0.3, gapSize: 0.2} );
    layer.addGameObject(eixosX);

    geoY.push(new THREE.Vector3(), new THREE.Vector3( 0, -17, 0 ));
    eixosY = ThreeJSBuilder.createLinesObject(0,0,0,geoY,0xFF000F);
    eixosY.threeObject.material = new THREE.LineDashedMaterial( { color: 0x00cc00, dashSize: 0.3, gapSize: 0.2} );
    layer.addGameObject(eixosY);

    geoZ.push(new THREE.Vector3(), new THREE.Vector3( 0, 0, -17 ));
    eixosZ = ThreeJSBuilder.createLinesObject(0,0,0,geoZ,0xFF000F);
    eixosZ.threeObject.material = new THREE.LineDashedMaterial( { color: 0x0000ff, dashSize: 0.3, gapSize: 0.2} );
    layer.addGameObject(eixosZ);

    layer.threeObject.name = "eixo_n";

	this.getEixos = function(){
		return layer;
	};
};
