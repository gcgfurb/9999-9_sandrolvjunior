
var CollaborationSystem = new function()
{
	this.fireSystemEventListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onSystemEvent", [evt]);
	};
	
	this.fireMessageListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onCollabMessage", [evt]);
	};
	
	this.fireOtherJoinedListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onOtherJoined", [evt]);
	};
	
	this.fireBoardDataListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onBoardData", [evt]);
	};
	
	this.fireDrawBoardListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onDrawBoard", [evt]);
	}

	this.fireClearBoardListener = function(evt)
	{
		ComponentUtils.fireComponentEvent(CollaborationSystem.getListName(), "onClearBoard", [evt]);
	}
	
	this.getTag = function(){
		return "COLLABORATION_SYSTEM";
	};

	this.getListName = function(){
		return "listComponentsCollaboration";
	};
}