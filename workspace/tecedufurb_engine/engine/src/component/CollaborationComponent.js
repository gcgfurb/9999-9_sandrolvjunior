//TODO:Documentação

function CollaborationComponent()
{
}

CollaborationComponent.prototype = new Component();

JSUtils.addMethod(CollaborationComponent.prototype, "initialize", function(collaborationStrategy, boxDiv)
{
	this.initialize();

	this.onOtherFunction = null;

	this.collaborationStrategy = collaborationStrategy;

	this.messageMap = new Array();

	this.fireEventFunction = ComponentUtils.fireComponentEvent;

	this.ignoredSystems =
		[
				LogicSystem.getListName(), CollaborationSystem.getListName()
		];

	this.setUp(boxDiv);

	return this;
});

CollaborationComponent.prototype.setUp = function(boxDiv)
{
	this.collaborationStrategy.setUp(this);
	this.createInterruption();

	/*
	var width = parseInt(boxDiv.style.width, 10);
	var height = parseInt(boxDiv.style.height, 10);

	 * var gameCanvas = boxDiv.firstElementChild;
	 * 
	 * gameCanvas.width = width; gameCanvas.height = height;
	 * gameCanvas.style.position = "absolute"; gameCanvas.style.left = 0;
	 * gameCanvas.style.top = 0;
	 */

	var boardCanvas = document.createElement("canvas");
	boxDiv.appendChild(boardCanvas);

	boardCanvas.id = "boardCanvas";
	boardCanvas.width = 900;
	boardCanvas.height = 700;
	boardCanvas.style.position = "absolute";
	boardCanvas.style.left = 0;
	boardCanvas.style.top = 0;
	boardCanvas.style.pointerEvents = "none";
	
	this.sketchBoard = new SketchBoard(boardCanvas, this.collaborationStrategy);
};

CollaborationComponent.prototype.addMessage = function(msg, func)
{
	ArrayUtils.putElement(this.messageMap, msg, func);
};

CollaborationComponent.prototype.sendMessage = function(msg, args)
{
	this.collaborationStrategy.sendMessage(msg, args);
};

CollaborationComponent.prototype.createInterruption = function()
{
	var self = this;

	ComponentUtils.fireComponentEvent = function(listName, functionStr, paramsArray)
	{
		if (listName === MouseSystem.getListName())
		{
			if (self.sketchBoard.preventEvent(functionStr, paramsArray))
				return;
		}

		self.fireEventFunction.apply(ComponentUtils, arguments);

		if (ArrayUtils.contains(self.ignoredSystems, listName))
		{
			return;
		}

		self.collaborationStrategy.sendEvent(listName, functionStr, paramsArray);
	};
};

CollaborationComponent.prototype.onSystemEvent = function(event, args)
{
	this.fireEventFunction.call(ComponentUtils, event.listName, event.functionStr, event.paramsArray);
};

CollaborationComponent.prototype.onCollabMessage = function(event)
{
	ArrayUtils.getElementByKey(this.messageMap, event.msg).apply(null, event.args);
};

CollaborationComponent.prototype.onOtherJoined = function(event)
{
	this.collaborationStrategy.sendBoardData(this.sketchBoard.lines);
	
	if (this.onOtherFunction != null)
	{
		this.onOtherFunction.call();
	}

};

CollaborationComponent.prototype.onBoardData = function(event)
{
	this.sketchBoard.reDraw(event.data);
};

CollaborationComponent.prototype.onDrawBoard = function(event)
{
	this.sketchBoard.draw(event.start, event.end, event.color, event.size, event.compositeOperation, true);
};

CollaborationComponent.prototype.onClearBoard = function(event)
{
	this.sketchBoard.clear(false);
};

CollaborationComponent.prototype.getSystems = function()
{
	var systems = new Array();
	systems = ArrayUtils.addElement(systems, CollaborationSystem);
	return systems;
};

CollaborationComponent.prototype.getTag = function()
{
	return "COLLABORATION_COMPONENT";
};