function TogetherJSUI(strategy, component) {
	this.strategy = strategy;
	this.component = component;
}

TogetherJSUI.prototype.setUp = function() {
	this.createCanvasButton();
	this.createCanvasToolBar();
};

TogetherJSUI.prototype.addButton = function(button) {
	document.getElementById("togetherjs-buttons").appendChild(button);

	var dock = document.getElementById("togetherjs-dock");
	dock.style.height = (parseInt(dock.style.height, 10) + 61) + "px";
};

TogetherJSUI.prototype.createCanvasButton = function() {
	var self = this;
	this.boardButton = document.createElement("button");
	this.boardButton.id = "visedu-board-button";
	this.boardButton.className = "togetherjs-button";

	this.boardButton.onclick = function() {
		var active = self.component.sketchBoard.toogleActive();

		 self.boardToolBarDiv.style.display = active ? "initial" : "none";
	};

	this.addButton(this.boardButton);
}

TogetherJSUI.prototype.createCanvasToolBar = function() {
	var self = this;

	this.boardToolBarDiv = document.createElement("div");
	document.body.appendChild(this.boardToolBarDiv);

	this.boardToolBarDiv.id = "visedu-board-toolbar-outter";

	this.boardToolBarDiv.style.display = "none";
	this.boardToolBarDiv.style.position = "fixed";
	this.boardToolBarDiv.style.bottom = "0px";
	this.boardToolBarDiv.style.width = "100%";

	var toolBarInner = document.createElement("div");
	this.boardToolBarDiv.appendChild(toolBarInner);

	toolBarInner.id = "visedu-board-toolbar-inner";

	toolBarInner.style.margin = "auto";
	toolBarInner.style.width = "40%";
	//toolBarInner.style.background = "red";

	var buttonBlack = document.createElement("button");
	toolBarInner.appendChild(buttonBlack);
	
	//buttonBlack.className = "togetherjs-button";
	buttonBlack.innerHTML = "Preto";

	buttonBlack.onclick = function() {
		self.component.sketchBoard.setColor("#000");
	};
	
	var buttonRed = document.createElement("button");
	toolBarInner.appendChild(buttonRed);
	
	buttonRed.innerHTML = "Vermelho";

	buttonRed.onclick = function() {
		self.component.sketchBoard.setColor("#F00");
	};
	
	var buttonClear = document.createElement("button");
	toolBarInner.appendChild(buttonClear);
	
	buttonClear.innerHTML = "Limpar";

	buttonClear.onclick = function() {
		self.component.sketchBoard.clear(true);
	};
	
	var buttonEraser = document.createElement("button");
	toolBarInner.appendChild(buttonEraser);
	
	buttonEraser.innerHTML = "Borracha";

	buttonEraser.onclick = function() {
		self.component.sketchBoard.setEraser();
	};

}