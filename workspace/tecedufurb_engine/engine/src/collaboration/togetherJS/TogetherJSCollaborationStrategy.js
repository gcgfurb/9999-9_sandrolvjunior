//TODO: Documentação
//Implementação da strategy de colaboração usando a API TogetherJS

function TogetherJSCollaborationStrategy()
{
}

TogetherJSCollaborationStrategy.prototype = new CollaborationStrategy();

TogetherJSCollaborationStrategy.prototype.setUp = function(component)
{
	TogetherJSConfig_dontShowClicks = true;
	TogetherJSConfig_disableWebRTC = true;
	TogetherJSConfig_cloneClicks = false;
	TogetherJSConfig_suppressJoinConfirmation = true;
	TogetherJSConfig_ignoreMessages = true;

	var self = this;
	TogetherJS.hub.on("systemEvent", CollaborationSystem.fireSystemEventListener);

	TogetherJS.hub.on("boardData", CollaborationSystem.fireBoardDataListener);

	TogetherJS.hub.on("message", CollaborationSystem.fireMessageListener);

	TogetherJS.hub.on("drawBoard", CollaborationSystem.fireDrawBoardListener);

	TogetherJS.hub.on("clearBoard", CollaborationSystem.fireClearBoardListener);

	TogetherJS.hub.on("togetherjs.hello", CollaborationSystem.fireOtherJoinedListener);

	TogetherJS(this);

	self.ui = new TogetherJSUI(this, component);

	TogetherJS.once("ready", function()
	{

		var shortenUrl = function()
		{
			var request = gapi.client.urlshortener.url.insert(
				{
					resource :
						{
							longUrl : TogetherJS.shareUrl()
						}
				});

			request.execute(function(response)
			{
				var shortUrl = response.id;
				console.log('short url:', shortUrl);
				var urlTextField = document.getElementsByClassName("togetherjs-share-link")[0];
				urlTextField.value = shortUrl;

				var shareDiv = document.getElementsByClassName("togetherjs-not-mobile")[0];
				var qrcode = new Image();

				qrcode.onload = function()
				{
					shareDiv.appendChild(qrcode);
				};

				qrcode.src = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + shortUrl;
			});
		};

		gapi.client.setApiKey("AIzaSyBts4giphQV-M01OHmN5J6xGp_uqUiMMrg")
		gapi.client.load("urlshortener", "v1", shortenUrl);

		self.ui.setUp();
	});
};

TogetherJSCollaborationStrategy.prototype.sendEvent = function(listName, functionStr, paramsArray)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "systemEvent",
				listName : listName,
				functionStr : functionStr,
				paramsArray : paramsArray
			});
	}
};

TogetherJSCollaborationStrategy.prototype.sendMessage = function(msg, args)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "message",
				msg : msg,
				args : args
			});
	}
};

TogetherJSCollaborationStrategy.prototype.sendBoardData = function(data)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "boardData",
				data : data
			});
	}
};

TogetherJSCollaborationStrategy.prototype.drawBoard = function(start, end, color, size, compositeOperation)
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "drawBoard",
				start : start,
				end : end,
				color : color,
				size : size,
				compositeOperation : compositeOperation
			});
	}
};

TogetherJSCollaborationStrategy.prototype.clearBoard = function()
{
	if (TogetherJS.running)
	{
		TogetherJS.send(
			{
				type : "clearBoard",
			});
	}
};