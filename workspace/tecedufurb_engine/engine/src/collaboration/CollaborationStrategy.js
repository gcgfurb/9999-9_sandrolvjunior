//TODO: Documentação
//interface para as Strategy das differentes APIs de colaboração

function CollaborationStrategy()
{
}

CollaborationStrategy.prototype.setUp = function()
{
};

CollaborationStrategy.prototype.sendEvent = function(listName, functionStr, paramsArray)
{
};

CollaborationStrategy.prototype.sendMessage = function(msg, args)
{
};

CollaborationStrategy.prototype.sendBoardData = function(data)
{
};

CollaborationStrategy.prototype.drawBoard = function(lastMouse, mouse, strokeStyle, lineWidth, globalCompositeOperation)
{
};

CollaborationStrategy.prototype.clearBoard = function()
{
};